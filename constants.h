#ifndef _constants_h_
#define _constants_h_

// General OR Common Constants
#define DEBUG				0	// 1 = Enabled; 0 = Disabled
#define ROUTER_PORT      		0xff
#define TRUE 				1
#define FALSE 				0
#define LINKUP 				1
#define LINKDOWN 			0
#define DEFAULT_STARTUP_JITTER 		1	// 1sec
#define DEFAULT_EXPIRATION_CHECK_PERIOD 1 	// 1sec... 
#define MAX_SEQUENCE_ID 		1000000 // For Packet Utils
#define MIN_PACKET_DROP_TIME		3	// 3 sec For 1-hop communication	[Not Used]
#define MAX_PACKET_DROP_TIME		10	// 10 sec For multi-hop communication	[Not Used]
#define IERP_TTL			50	// Maximum-length for IERP route

// NDP related Constants
#define DEFAULT_BEACON_PERIOD 		3 	// 3 sec
#define DEFAULT_BEACON_PERIOD_JITTER	1 	// 1 sec
#define DEFAULT_NEIGHBOR_ACK_TIMEOUT  	2 	// 2 sec  [Within this much time ACK should come to me]
#define DEFAULT_MAX_ACK_TIMEOUT		2	// How many ACK Timeout is needed to declare a Neighbor DOWN

// IARP related Constants
#define DEFAULT_MIN_IARP_UPDATE_PERIOD 	3 	// 3 sec  [T_lsu]
#define DEFAULT_MAX_IARP_UPDATE_PERIOD 	10 	// 10 sec [T_lsu]
#define DEFAULT_LINK_LIFETIME 		10	// 10sec	
#define DEFAULT_UPDATE_LIFETIME 	30	// 30sec  [For Control Flooding]

// IERP related Constants
#define DEFAULT_BRP_XMIT_POLICY		0	// 1: BRP_MULTICAST, 0: BRP_UNICAST
#define IERP_REPLY_SNOOP		1	// 1: Enabled, 0: Disabled
#define IERP_ERROR_SNOOP		1	// 1: Enabled, 0: Disabled
#define IERP_XMIT_JITTER		1	// 1sec [Uniformly distributed]
#define DEFAULT_QUERY_LIFETIME 		30 	// 30sec  [For Control Flooding]
#define DEFAULT_QUERY_RETRY_TIME	5	// 5sec
#define DEFAULT_ROUTE_LIFETIME 		120	// 120sec  [IERP Route Reliability]
#define DEFAULT_MAX_IERP_REPLY		3	// Destination can send maximum 5 replies...

// ZRP related Constants
#define ZRP_DEFAULT_HDR_LEN		10	// 10 bytes [refer to hdr_zrp struct]
#define DEFAULT_ZONE_RADIUS 		1
#define DEFAULT_SEND_BUFFER_SIZE	100	// 100 Packets
#define DEFAULT_PACKET_LIFETIME		20	// 10 sec
#define	DEFAULT_INTERPKT_JITTER		1	// 1sec [Uniformly distributed]

// Type-Defintions & Enumeration...
typedef double Time;   
typedef int32_t Query_ID;

// Types of ZRP packets...
enum ZRPTYPE {
  	NDP_BEACON, NDP_BEACON_ACK, IARP_UPDATE, IARP_DATA, IERP_REPLY, IERP_REQUEST, IERP_ROUTE_ERROR, IERP_DATA
};

// BRP Xmit Policy...
enum BRP_XMIT_POLICY {
	BRP_UNICAST, BRP_MULTICAST
};

#endif
