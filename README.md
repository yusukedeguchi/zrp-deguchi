（概要）
zrp_previous.cc、zrp_propose.cは私の卒業研究のテーマ「モバイルアドホックネットワークにおけるトポロジ変化を考慮したゾーンルーチングプロトコル」で実装した先行研究、提案手法それぞれのプロトコルのC++のソースファイルである。

（実行方法）
1：NS2の公式サイトでNS2をインストールする。
（URL）https://sourceforge.net/projects/nsnam/

2:NS2にZRPを組み込むために、NS2のバージョンに適応するZRPのパッチをダウンロードする。
（URL）https://www.linuxquestions.org/questions/linux-wireless-networking-41/how-to-add-zrp-protocol-patch-in-ns2-v-2-35-in-linux-mint-4175624205/

3:端末を開く　

4:ソースファイルをコンパイル

5:シナリオファイルを指定し、実行（シナリオファイルとしてzrp-Demo1-1.tclをアップロードしている）
